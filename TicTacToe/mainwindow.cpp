 #include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QPixmap>
#define TAB 30
#define LINE 2
#define HEIGHT 540
#define WIDTH 800

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setStyleSheet("MainWindow { background-color : #172E49;}");
    ui->label_game->setStyleSheet("QLabel {color: white}");
    ui->label_level->setStyleSheet("QLabel {color: white}");
    ui->label_tab->setStyleSheet("QLabel {color: white}");
    ui->label_turn->setStyleSheet("QLabel {color: white}");
    tam = TAB;
    //ui->board->setEnabled(false);
    ui->board->setStyleSheet("QFrame { background-color : black;}");
    ui->pushButton_reset->setEnabled(false);
    int height, width_board, width_settings;

    height = HEIGHT-40;
    width_board = height;

    width_settings = WIDTH-width_board-50;
    this->setFixedSize(WIDTH,HEIGHT);

    ui->board->setGeometry(20,20,width_board,width_board);
    ui->settings->setGeometry(30+width_board,20,width_settings,height);



}

void MainWindow::init()
{
    int height, width_board, width_settings, width_board_normalized, tam_c;
    tam = ui->sizeDisplay->value();
    height = HEIGHT-40;
    width_board = height;

    width_settings = WIDTH-width_board-50;
    tam_c = (width_board-LINE*(tam+1))/tam;
    width_board_normalized = tam_c*tam+LINE*(tam+1);
    this->setFixedSize(WIDTH,HEIGHT);

    ui->board->setGeometry(20,20,width_board_normalized,width_board_normalized);
    ui->settings->setGeometry(30+width_board,20,width_settings,height);



    display = new Label*[tam*tam];
    for(int i = 0 ; i< tam ; i++)
    {
        for(int j = 0 ; j <tam ; j++ )
        {
            display[i*tam+j] = new Label(i*tam+j,ui->board);
            display[i*tam+j]->setStyleSheet("QLabel { background-color : white; color : black; }");
            display[i*tam+j]->setGeometry(LINE*(j+1)+j*tam_c,LINE*(i+1)+i*tam_c,tam_c,tam_c);
            connect(display[i*tam+j], SIGNAL( clicked(Label*) ), this, SLOT(setLabel(Label*)));
            display[i*tam+j]->show();
        }
    }

    states = new bool[tam*tam];

    for(int i = 0 ; i<tam*tam ; i++)
    {
        states[i] = 0;
    }

    if(ui->comboBox_turn->currentText()=="PC")
    {
        game = new TicTacToe(ui->sizeDisplay->value(),ui->sizeLine->value(), ui->level->value(),true);
        pair<int,int> g = game->next_move();
        int current_id = g.first*tam + g.second;
        QPixmap pix("p2.svg");
        int w = display[current_id]->width();
        int h = display[current_id]->height();

        display[current_id]->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
        states[current_id]=true;
    }
    else
    {
        game = new TicTacToe(ui->sizeDisplay->value(),ui->sizeLine->value(), ui->level->value(),false);
    }




}

void  MainWindow::setLabel(Label* label)
{
    int current_id = label->get_id();
    if(!states[current_id])
    {
        QPixmap pix("p1.svg");
        int w = label->width();
        int h = label->height();
        int i = current_id/tam;
        int j = current_id - i*tam;
        game->setPlayer(i,j);

        label->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
        states[current_id]=true;
        if(game->game_ended())
        {
            QMessageBox::information(this,"WIN","FELICITACIONES!!");
            return;
        }
        if(game->boardIsFull())
        {
            QMessageBox::information(this,"EMPATE","empate");
            return;
        }

        pair<int,int> g = game->next_move();
        int current_id = g.first*tam + g.second;
        QPixmap pix2("p2.svg");
        w = display[current_id]->width();
        h = display[current_id]->height();

        display[current_id]->setPixmap(pix2.scaled(w,h,Qt::KeepAspectRatio));
        states[current_id]=true;
        if(game->game_ended())
        {
            QMessageBox::information(this,"LOSE","USTED PERDIÓ");
            return;
        }

        if(game->boardIsFull())
        {
            QMessageBox::information(this,"EMPATE","empate");
            return;
        }

    }
    else
    {
        QMessageBox::information(this,"e__e","Jugada ya realizada");
    }
    cout<<"ID:"<<current_id<<endl;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_run_clicked()
{
    if(ui->sizeLine->value() <= ui->sizeDisplay->value() )
    {
        init();
        ui->pushButton_run->setEnabled(false);
        ui->sizeDisplay->setEnabled(false);
        ui->sizeLine->setEnabled(false);
        ui->level->setEnabled(false);
        ui->comboBox_turn->setEnabled(false);
        ui->pushButton_reset->setEnabled(true);
    }
    else
    {
        QMessageBox::information(this,"Warning","El tamaño de Tic Tac Toe debe ser menor o igual al tamaño del tablero.");
    }

}


void MainWindow::on_pushButton_reset_clicked()
{
    ui->pushButton_reset->setEnabled(false);
    ui->pushButton_run->setEnabled(true);
    ui->sizeDisplay->setEnabled(true);
    ui->sizeLine->setEnabled(true);
    ui->level->setEnabled(true);
    ui->comboBox_turn->setEnabled(true);
    for(int i = 0 ; i< tam*tam ; i++)
    {
        delete display[i];
    }
    delete [] display;
    //delete game;
}
