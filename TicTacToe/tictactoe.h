#ifndef TICTACTOE_H
#define TICTACTOE_H
#include <utility>
#include <vector>
#include <stdio.h>
#include <limits>

using namespace std;

class TicTacToe
{
    class Node
    {
    public:

        friend class TicTacToe;
        Node(int tam_board_p, int tam_line_p,int cont_p) : tam_board(tam_board_p),tam_line(tam_line_p)
                                                            ,cont(cont_p), board(new char*[tam_board_p])
        {
            //cout<<".."<<tam_board<<".."<<tam_line<<"..";
            for(int i = 0 ; i<tam_board ; i++)
            {
                board[i] = new char[tam_board];
                for(int j = 0 ; j<tam_board ; j++)
                {
                    board[i][j] = 0;
                }
            }

        }

        Node(int i,int j, char val, Node* &parent,int tam_board_p, int tam_line_p, int cont_p)
            : last_i(i),last_j(j),tam_board(tam_board_p), tam_line(tam_line_p), cont(cont_p)
                ,board(new char*[tam_board_p])
        {
            //cout<<"tamaño: "<<tam_board<<endl;
            //cout<<".."<<tam_board<<".."<<tam_line<<"..";
            for(int i = 0 ; i<tam_board ; i++)
            {
                board[i] = new char[tam_board];
                for(int j = 0 ; j<tam_board ; j++)
                {
                    board[i][j] = parent->board[i][j];
                }
            }
            board[last_i][last_j] = val;

        }

        ~Node()
        {
            
            vector<Node*>::iterator it;
            for (it = childs.begin(); it != childs.end(); ++it){
                delete *it;
                //cout<<i<<"\n\tdestructor Node\n";
            }
            childs.clear();

            for(int i = 0 ; i< tam_board ; i++)
            {
                delete [] board[i];
            }
            delete [] board;

        }



        int find_row(int i, int j)
        {
            int score = 1;
            int player = board[i][j];
            int p =0;
            int k;
            for(k = j+1 ; k<tam_board && board[i][k] ==player;k++)
            {
                score++;
            }
            if(k<tam_board && board[i][k] ==0)  p++;

            for(k = j-1 ; k>=0 && board[i][k] ==player;  k--)
            {
                score++;
            }
            if( k>=0 && board[i][k] ==0)    p++;

            if(score == tam_line)   return INT_MAX/5;

            //cout<<"row: "<<p*score<<endl;
            if(p)   return p*score;
            return score;
        }

        int find_col(int i, int j)
        {
            int score = 1;
            int player = board[i][j];
            int p = 0;
            int k;
            for(k = i+1 ; k<tam_board && board[k][j] ==player;k++)  score++;
            if(k<tam_board && board[k][j] == 0) p++;

            for(k = i-1 ; k>=0 && board[k][j] ==player;  k--)   score++;
            if(k>=0 && board[k][j] == 0) p++;
            if(score == tam_line)   return INT_MAX/5;
            //cout<<"col: "<<p*score<<endl;
            if(p)   return p*score;
            return score;
        }


        int find_dia(int i, int j)
        {
            int total_score =0;
            int score = 1;
            int player = board[i][j];
            int p = 0;
            int k,l;
            for(k = i-1,l = j-1 ; k>=0 && l>=0 && board[k][l] ==player ; k--,l--)   score++;
            if(k>=0 && l>=0 && board[k][l] ==0) p++;

            for(k = i+1,l = j+1 ; k<tam_board && l<tam_board && board[k][l] ==player ; k++,l++)   score++;
            if(k<tam_board && l<tam_board && board[k][l] ==0)   p++;

            if(score == tam_line)   return INT_MAX/5;
            if(p)   total_score =p*score;
            else    total_score = score;




            score = 1;
            p = 0;
            for(k = i-1,l = j+1 ; k>=0 && l<tam_board && board[k][l] ==player ; k--,l++)   score++;
            if(k>=0 && l<tam_board && board[k][l] ==0)  p++;

            for(k = i+1,l = j-1 ; k<tam_board && l>=0 && board[k][l] ==player ; k++,l--)   score++;
            if( k<tam_board && l>=0 && board[k][l] ==0)    p++;

            if(score == tam_line)   return INT_MAX/5;

            if(p)   total_score += p*score;
            else    total_score += score;

            //cout<<"dia: "<<total_score<<endl;
            return total_score;
        }


        int get_score(bool minmax)//min->false, max->true
        {
            if(gameIsEnded())
            {
                int temp[2] = {-1,1};
                val = INT_MAX*temp[!minmax];
                //cout<<"\n-------------GAME END FOUND: "<<val<<"\tPROFUNDIDAD: "<<cont<<"--------------\n";


                return INT_MAX*temp[!minmax];
            }
            if(childs.size()!=0)
            {
                string msg;
                if(minmax)
                {
                    msg = "max: ";
                }
                else
                {
                    msg = "min: ";
                }
                int mayor = childs[0]->get_score(!minmax);
                //cout<<msg<< mayor<<"\t";
                for(unsigned int i = 1 ; i<childs.size() ; i++)
                {
                    int temp = childs[i]->get_score(!minmax);
                    //cout<<msg<< temp<<"\t";
                    if(minmax)
                    {
                        if(temp >mayor) mayor = temp;
                    }
                    else
                    {
                        if(temp<mayor)  mayor = temp;
                    }
                }
                val = mayor;
                //cout<<"\n\n";

            }

            else
            {
                int temp[2] = {-1,1};
                int score =0;
                for(int i = 0 ; i<tam_board ; i++)
                {
                    for(int j = 0 ; j<tam_board ; j++)
                    {
                        if(board[i][j] != 0)
                        {
                            int value = temp[board[i][j]-1];
                            score+=(find_row(i,j)*value);
                            score+=(find_col(i,j)*value);
                            score+=(find_dia(i,j)*value);
                        }
                    }
                }
                val = score;
                //cout<<"nodo terminal: "<<val<<"\n";

            }
            return val;
        }

        bool gameIsEnded()
        {
            int winner_score = INT_MAX/5;
            for(int i = 0 ; i<tam_board ; i++)
            {
                for(int j = 0 ; j<tam_board ; j++)
                {

                    if( board[i][j] != 0 &&(
                            find_row(i,j)== winner_score
                            || find_col(i,j) == winner_score
                            || find_dia(i,j)== winner_score))   return true;
                }
            }
            return false;
        }


        vector< pair<int,int> > generate_moves()
        {
            vector< pair<int,int> > res;
            for(int i = 0 ; i<tam_board ; i++)
            {
                for(int j = 0 ; j<tam_board ;j++)
                {
                    if(board[i][j]==0)
                    {
                        pair<int,int> temp(i,j);
                        res.push_back(temp);
                    }
                    
                }
            }
            return res;
        }


        void addChild(Node* &newChild)
        {
            childs.push_back(newChild);
        }

        int get_cont()
        {
            return cont;
        }

        void setElement(int i, int j, char value)
        {
            board[i][j] = value;
        }


        void print()
        {
            for (int i = 0 ; i< tam_board ; i++)
            {
                for(int j = 0 ; j<tam_board ; j++)
                {
                    if(board[i][j] == 1)
                    {
                        cout<<1<<"\t";
                    }
                    else
                    {
                        if(board[i][j] == 2)
                        {
                            cout<<2<<"\t";
                        }
                        else
                        {
                            cout<<0<<"\t";
                        }
                    }
                    
                }
                cout<<endl;
            }
            cout<<"\n\n";
        }

    private:
        int last_i;
        int last_j;
        int val;
        int tam_board;
        int tam_line;
        vector<Node*> childs;
        char** board;
        int cont;
    };


public:
    TicTacToe(int board = 3, int line = 3, int deep_p=5, bool first= true) : deep(deep_p)
                                                            ,tam_line(line),tam_board(board)
                                                            ,current(new Node(board,line,0))
    {
        players[0] = 1;
        players[1] = 2;
        if(first)
        {
            generate_tree(current,1);
            //cout<<"score: "<<current->get_score(true);
        }

    }

    ~TicTacToe()
    {
    	cout<<"\n\tdestructor tic tac toe\n";
        delete current;
    }


    void generate_tree(Node* &p,bool player_p, int current_deep = 0)
    {
        if(current_deep==deep)
        {
            //p->print();
            return;
        }
        vector< pair<int,int> > moves = p->generate_moves();

        for(unsigned int i = 0 ; i< moves.size() ; i++)
        {
            //printf("i: %d\tj: %d\n",moves[i].first,moves[i].second);
            Node* temp = new Node(moves[i].first,moves[i].second,players[player_p],p,tam_board,tam_line,p->cont+1);
            //temp->print();
            p->addChild(temp);
            generate_tree(temp,!player_p,current_deep+1);
        }
    }


    pair<int,int> next_move()
    {
        //cout<<"\n\n\n NUEVA JUGADA\n";
        current->get_score(true);
        int max = current->childs[0]->val;
        int pos = 0;
        for( unsigned int i = 1 ; i< current->childs.size() ; i++)
        {
            if(current->childs[i]->val>max)
            {
                max = current->childs[i]->val;
                pos = i;
            }
        }
        //cout<<"\nMAX:" <<max<<"\n";
        current->board[current->childs[pos]->last_i][current->childs[pos]->last_j] = 2;
        current->cont++;
        return pair<int,int>(current->childs[pos]->last_i, current->childs[pos]->last_j );
    }

    void setPlayer(int i, int j)
    {
        current->setElement(i,j,1);
        current->cont++;
        for(unsigned int i = 0 ; i< current->childs.size() ; i++)
        {
            delete current->childs[i];
        }
        current->childs.clear();
        generate_tree(current,1);

    }

    int getDeep()
    {
        return deep;
    }

    bool game_ended()
    {
        return current->gameIsEnded();
    }

    bool boardIsFull()
    {
        if(current->cont == tam_board*tam_board)    return true;
        return false;
    }


private:
    Node* current;
    int deep;
    int tam_board;
    int tam_line;
    char players[2];

};


#endif // TICTACTOE_H
