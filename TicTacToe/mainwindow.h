#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include "label.h"
#include "tictactoe.h"

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Label** display;
    bool* states;
    int tam;
    TicTacToe* game;

    void init();

public slots:
    void setLabel(Label* label);

private slots:
    void on_pushButton_run_clicked();
    void on_pushButton_reset_clicked();
};

#endif // MAINWINDOW_H
