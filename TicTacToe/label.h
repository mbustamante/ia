#ifndef LABEL_H
#define LABEL_H

#include <QLabel>
#include <QMouseEvent>

class Label : public QLabel
{
    Q_OBJECT
public:
    Label(QWidget *parent = 0, Qt::WindowFlags f=0);
    Label(const int id, QWidget *parent=0,Qt::WindowFlags f=0);
    Label(const QString &text, QWidget *parent=0,Qt::WindowFlags f=0);
    Label(const int id, const QString &text,QWidget *parent=0,Qt::WindowFlags f=0);
    int get_id();
    ~Label();

protected:
    void mousePressEvent(QMouseEvent *ev);
    int id_pos;

signals:
    void clicked(Label* label);

public slots:
    //void setLabel(Label* label);

};

#endif // LABEL_H
