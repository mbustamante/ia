#include "label.h"

Label::Label(QWidget *parent, Qt::WindowFlags f):QLabel(parent,f)
{

}

Label::Label(const QString &text, QWidget *parent, Qt::WindowFlags f):QLabel(text,parent,f)
{

}

Label::Label(const int id, QWidget *parent, Qt::WindowFlags f):QLabel(parent,f)
{
    id_pos = id;
}

Label::Label(const int id, const QString &text, QWidget *parent, Qt::WindowFlags f):QLabel(text,parent,f)
{
    id_pos = id;
}



Label::~Label(){

}


int Label::get_id()
{
    return id_pos;
}


void Label::mousePressEvent(QMouseEvent *ev){
    emit clicked(this);
}
