#-------------------------------------------------
#
# Project created by QtCreator 2014-10-12T18:36:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TicTacToe
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    label.cpp

HEADERS  += mainwindow.h \
    label.h \
    tictactoe.h

FORMS    += mainwindow.ui
