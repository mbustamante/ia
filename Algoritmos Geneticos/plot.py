# -*- coding:utf-8 -*-


import matplotlib.pyplot as plt

a=[[0,0],[1,1],[2,4],[3,9],[4,16],[5,25],[6,36],[7,49]]
plt.plot(*zip(*a), marker='o', color='r')
plt.show()