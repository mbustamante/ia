# -*- coding:utf-8 -*-

import math
from math import log, floor
from random import randrange, random
import matplotlib.pyplot as plt


def read_file(p_file):
    try:
        dirFile = open(p_file)
        data = dirFile.read()
        dirFile.close()
        return data
    except:
        print("no se encontro el directorio")


def bit_leng(n):
	return int(floor(log(n,2))+1)

def crossing(first, second, lenght):
	cut = randrange(1,lenght-1)
	str_base = '{0:0'+str(lenght) +'b}'
	bin_first =	str_base.format(first)
	bin_second = str_base.format(second)

	temp = bin_first
	bin_first =bin_first[:cut]+bin_second[cut-lenght:]
	bin_second = bin_second[:cut]+temp[cut-lenght:]
	return (int(bin_first,2) , int(bin_second,2))

def mutation(first,  lenght):

	n = randrange(1,lenght-2)
	positions = [randrange(lenght) for i in range(n)]
	
	str_base = '{0:0'+str(lenght) +'b}'
	dict_change = {'1':'0', '0':'1'}
	bin_first = list(str_base.format(first))
	# print "\n\n",first
	# print bin_first
	# print "positions ->> ",positions
	for position in positions:
		# print "----",position
		bin_first[position] = dict_change[bin_first[position]]


	return int("".join(bin_first),2)


def get_positions(population_aptitude):
	s = population_aptitude
	return sorted(range(len(s)), key=lambda k: s[k])

def get_selection_ranking(n):
	total_sum = reduce(lambda a,b: a+b, range(1,n+1))
	ranking = [ i/float(total_sum) for i in range(1,n+1)]
	return ranking

def selection_ranking(ranking):
	
	a = 0
	b = 0
	R = random()
	sum_temp = 0
	for i in range(len(ranking)):
				sum_temp += ranking[i]
				if sum_temp>R:
					a = i
					break
	R = random()
	sum_temp = 0
	for i in range(len(ranking)):
				sum_temp += ranking[i]
				if sum_temp>R:
					b = i
					break

	return (a , b)


def minimization(t_population, p_crossing, p_mutation, function, n_generations, min_max,elitism):
	x_max = min_max['x'][1]
	x_max_tam = bit_leng(x_max)
	y_max = min_max['y'][1]
	y_max_tam = bit_leng(y_max)
	z_max = min_max['z'][1]
	z_max_tam = bit_leng(z_max)

	population = {'x':[randrange(min_max['x'][0], x_max+1) for i in range(t_population)], 
					'y':[randrange(min_max['y'][0], y_max+1) for i in range(t_population)], 
					'z':[randrange(min_max['z'][0], z_max+1) for i in range(t_population)]
				}
	population_aptitude = []
	for i in range(t_population):
		x = population['x'][i]
		y = population['y'][i]
		z = population['z'][i]
		population_aptitude.append(eval(function))

	
	
	vector_averages = []
	vector_minimuns = []
	generations = 1;
	ranking = get_selection_ranking(t_population)
	while generations<=2*n_generations:
		total_sum = reduce(lambda a,b: a+b, population_aptitude)
		average = total_sum/float(t_population)
		minimun = reduce(lambda a,b: a if (a<b) else b, population_aptitude)
		vector_minimuns.append([generations,minimun])
		vector_averages.append([generations,average])

		positions = get_positions(population_aptitude)

		new_population = {'x':[], 'y':[], 'z':[]}
		new_population_aptitude = []
		while(len(new_population['x'])< 2*t_population):
			probability = random()
			if(probability<=p_mutation):
				selected_elements = selection_ranking(ranking)
				new_element = mutation(population['x'][positions[selected_elements[0]]]
								,x_max_tam)
				new_population['x'].append(new_element)

				new_element = mutation(population['y'][positions[selected_elements[0]]]
								,y_max_tam)
				new_population['y'].append(new_element)

				new_element = mutation(population['z'][positions[selected_elements[0]]]
								,z_max_tam)
				new_population['z'].append(new_element)


			if(probability<=p_crossing):
				selected_elements = selection_ranking(ranking)
				new_element = crossing(population['x'][positions[selected_elements[0]]]
									,population['x'][positions[selected_elements[1]]]
									,x_max_tam)
				new_population['x'].append(new_element[0])
				new_population['x'].append(new_element[1])

				new_element = crossing(population['y'][positions[selected_elements[0]]]
									,population['y'][positions[selected_elements[1]]]
									,y_max_tam)
				new_population['y'].append(new_element[0])
				new_population['y'].append(new_element[1])

				new_element = crossing(population['z'][positions[selected_elements[0]]]
									,population['z'][positions[selected_elements[1]]]
									,z_max_tam)
				new_population['z'].append(new_element[0])
				new_population['z'].append(new_element[1])

		for i in range(len(new_population['x'])):
			x = new_population['x'][i]
			y = new_population['y'][i]
			z = new_population['z'][i]
			new_population_aptitude.append(eval(function))

		positions_new = get_positions(new_population_aptitude)[:t_population-elitism]


		temp_population = population.copy()
		temp_population_aptitude = population_aptitude

		population_aptitude = []
		population['x'] = []
		population['y'] = []
		population['z'] = []

		for position in positions[:elitism]:
			population_aptitude.append(temp_population_aptitude[position])
			population['x'].append(temp_population['x'][position])
			population['y'].append(temp_population['y'][position])
			population['z'].append(temp_population['z'][position])

		
		for position in positions_new:
			population_aptitude.append(new_population_aptitude[position])
			population['x'].append(new_population['x'][position])
			population['y'].append(new_population['y'][position])
			population['z'].append(new_population['z'][position])

		# print "\n\n",population
		# print population_aptitude
		print "MINIMO: ",minimun
		generations+=1
	x = zip(*vector_averages)[0]
	y_min = zip(*vector_minimuns)[1]
	y_avg = zip(*vector_averages)[1]
	plt.plot(x,y_min,marker='o',color='r')
	plt.plot(x, y_avg,color='b')
	
	plt.show()

	return reduce(lambda a,b: a if (a<b) else b, population_aptitude)





if __name__ == '__main__':
	x = 15
	y = 33
	l = bit_leng(y)
	p_mutation = 0.30
	p_crossing = 0.80
	elitism = 3
	function = 'pow(x,3)+pow(y,3)+pow(z,3)'
	min_max = {'x':[0,311], 'y':[0,255], 'z':[0,145]}
	print minimization(10,p_crossing,p_mutation,function,100,min_max,elitism)